# Enforce the type
def translate(my_int: int) -> str:
    str_int = str(my_int)
    new_string = ''
    for i, n in enumerate(str_int[::-1]):
        if i % 3 == 0 and n != '-':
            new_string = ',' + new_string
        new_string = n + new_string
    return new_string[:-1]


if __name__ == "__main__":
    # Some examples:
    print (translate(12354893))
    print (translate(1))
    print (translate(129134237424))
    print (translate(-1291342344))
    print (translate(0))

    # type checker won't like this:
    print (translate('a'))
